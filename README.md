# Continuous Improvement Tools and Methods

These materials are training materials for the Continuous Improvement Workshop and learning the Lean philosophy of improvement. Learning materials are grouped in 12 - 3hr. sessions.

Using the tools in the order by session will walk you through the lean improvement and problem solving method. PowerPoint presentations of the individual tools and methods are included for quick reference and learning.